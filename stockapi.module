<?php

/**
 * @file
 *  API for retrieving stock information from Alpha Vantage.
 */
// TODO: Add historical data option.
// TODO: Updates to default stockapi symbols

module_load_include('inc', 'stockapi');

/**
 * Implements hook_menu().
 */
function stockapi_menu() {
  $items = array();
  $items['stockapi'] = array(
    'title' => 'Stock API',
    'description' => 'Stock Quotes',
    'page callback' => 'stockapi_page',
    'access arguments' => array('access content'),
  );

  $items['admin/config/services/stockapi'] = array(
    'title' => 'Stock API settings',
    'description' => 'Configure settings for StockAPI',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stockapi_admin_settings'),
    'access arguments' => array('administer site configuration'),
  );
  
  return $items;
}

/**
 * Implements hook_cron().
 */
function stockapi_cron() {
  if (REQUEST_TIME - variable_get('stockapi_fetch_last', 0) >= variable_get('stockapi_fetch', 3600)) {
    if (intval(variable_get('stockapi_fetch_first', 0)) < 1) {
      stockapi_fetch_symbols();
    }
    else {

      $symbols = array();
      $result = db_query('SELECT symbol FROM {stockapi} ORDER BY updated');
      if (count($result)) {
    foreach ($result as $data) {
      $symbols[] = $data->symbol;
    }
      }

      $default_symbols = variable_get('stockapi_default_symbols', '');
      if (drupal_strlen($default_symbols)) {
        $default_symbols_array = explode(' ', $default_symbols);
        $symbols = array_unique(array_merge($default_symbols_array, $symbols));
      }

      if (count($symbols)) {
    $stocks = stockapi_fetch($symbols);
    foreach ($stocks as $key => $stock) {
      stockapi_save(_stockapi_to_object($stock));
    }
    module_invoke_all('stockapi_post_update', $stocks);
    variable_set('stockapi_fetch_last', REQUEST_TIME);
    cache_clear_all('variables', 'cache');
  }
}
  }
}

/**
 * Create the form for Stock API admin settings.
 */
function stockapi_admin_settings() {

  $period = drupal_map_assoc(array(900, 1800, 3600, 21600, 43200, 86400), 'format_interval');

  $form = array();
  $form['stockapi_fetch'] = array(
    '#type' => 'select',
    '#title' => t('Stock data update frequency'),
    '#default_value' => variable_get('stockapi_fetch', 3600),
    '#options' => $period,
    '#description' => t('How often to refresh the stock data from Alpha ' .
      'Vantage Realtime and historical equity data. Default is 1 hour' .
      ' (3600 seconds).'),
  );

  $quotetype = array('basic' => 'basic', 'extended' => 'extended', 'realtime' => 'realtime');
  $form['stockapi_quotetype'] = array(
    '#type' => 'select',
    '#title' => t('Select the type of quotes to retrieve'),
    '#default_value' => variable_get('stockapi_quotetype', 'basic'),
    '#options' => $quotetype,
    '#description' => t('Quote type: basic, extended, or realtime'),
  );

  $form['stockapi_default_symbols'] = array(
    '#type' => 'textarea',
    '#title' => t('Default symbols'),
    '#default_value' => variable_get('stockapi_default_symbols', implode(' ', stockapi_get_default_symbols())),
    '#description' => t('Enter symbols to fetch quotes for, each spearated by a space'),
  );

  $form['stockapi_decimals'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of decimals'),
    '#default_value' => variable_get('stockapi_decimals', 2),
    '#description' => t('Number of decimals displayed after the point.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['stockapi_vantage_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('The <a href="@alphavantage">API key</a> used for accessing' .
      ' <a href="@alphavantage">json alphavantage stock data</a>.  "demo" ' .
      ' is only good for stocks like AAPL and MSFT (see description).',
      array('@alphavantage' => 'https://www.alphavantage.co/support/#api-key',
      )),
    '#default_value' => variable_get('stockapi_vantage_apikey', 'demo'),
    '#description' => t('Please <a href="@alphavantage">register for your free stock ' .
     'API key</a> demo is only good for stocks like AAPL and MSFT.  Alphavantage ' .
     ' asks that we not do more than one request per second.  Please do not' .
     ' tamper with the default request frequency settings. ' .
     '<a href="@termsofservice">See terms and conditions.</a>',
      array(
        '@alphavantage' => 'https://www.alphavantage.co/support/#api-key',
        '@termsofservice' => 'https://www.alphavantage.co/terms_of_service/',
      )),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 *  Create the basic StockAPI page.
 */
function stockapi_fetch_symbols() {
  if (intval(variable_get('stockapi_fetch_first', 0)) < 1) {
    $symbols = array();
    $default_symbols = variable_get('stockapi_default_symbols', '');
    if (drupal_strlen($default_symbols)) {
      $symbols = explode(' ', $default_symbols);
    }
    if (!count($symbols)) {
      $symbols = stockapi_get_default_symbols();
    }
    $stocks = stockapi_fetch($symbols);
    foreach ($stocks as $stock) {
      $stock = _stockapi_to_object($stock);
      stockapi_save($stock);
    }
    module_invoke_all('stockapi_post_update', $stocks);
    variable_set('stockapi_fetch_first', REQUEST_TIME);
    watchdog('stockapi', sprintf("Stock API has successfully loaded all default symbols: %s", strftime('Y-m-d H:i:s', REQUEST_TIME)));
  }
  }

/**
 * Helper function to get default symbols for Stock API
 *
 * @return array
 */
function stockapi_get_default_symbols() {
  return array('IBM', 'AAPL', 'AMZN', 'DIS', 'GOOG', 'HPQ', 'INTC', 'GE', 'NMX', 'AGB', 'FVI');
}

/**
 * List of all default stock quotes
 *
 */
function stockapi_page() {
  drupal_add_css(drupal_get_path('module', 'stockapi') . '/stockapi.css');
  $header = array(
    'symbol' => array('data' => t('Symbol'), 'field' => 's.symbol', 'class' => array('left')),
    'name' => array('data' => t('Name'), 'field' => 's.name', 'class' => array('left')),
    'last_trade_price_only' => array('data' => t('Last'), 'field' => 's.last_trade_price_only', 'class' => array('right')),
    'chg' => array('data' => t('Change'), 'field' => 's.chg', 'class' => array('right')),
    'pct_chg' => array('data' => t('%'), 'field' => 's.pct_chg', 'class' => array('right')),
    'open' => array('data' => t('Updated'), 'field' => 's.open', 'class' => array('right')),
    'volume' => array('data' => t('Volume'), 'field' => 's.volume', 'class' => array('right')),
    'last_trade_date' => array('data' => t('Date'), 'field' => 's.last_trade_date', 'class' => array('right')),
    'last_trade_time' => array('data' => t('Time'), 'field' => 's.last_trade_time', 'class' => array('right')),
  );

  $query = db_select('stockapi', 's')->extend('PagerDefault')->extend('TableSort');
  $result = $query->fields('s', array('symbol', 'name', 'last_trade_price_only', 'chg', 'pct_chg', 'open', 'volume', 'last_trade_date', 'last_trade_time', 'exchange'))
      ->limit(50)
      ->orderByHeader($header)
      ->execute();

  $rows = array();

  while ($data = $result->fetchObject()) {
    $rows[] = array(
      stockapi_format_symbol($data->symbol, $data->name, $data->exchange),
      $data->name,
      stockapi_format_align(stockapi_format_decimals($data->last_trade_price_only)),
      stockapi_format_align(stockapi_format_change(stockapi_format_decimals($data->chg))),
      stockapi_format_align(stockapi_format_change(stockapi_format_decimals($data->pct_chg))),
      stockapi_format_align(stockapi_format_decimals($data->open)),
      stockapi_format_align($data->volume),
      $data->last_trade_date,
      $data->last_trade_time
    );
  }

  $output = theme_table(array('header' => $header, 'rows' => $rows, 'attributes' => array('width' => '100%', 'id' => 'stockapi-table'), 'sticky' => TRUE, 'caption' => t('All available stock quotes.'), 'colgroups' => array(), 'empty' => t('No stock quotes available.'))) . theme("pager");

  return $output;
}

/**
 * Generate link to full quote for the stock sybol
 *
 * @param string $field
 * @return string
 */
function stockapi_format_symbol($field, $title = '', $exchange = NULL) {
  if (empty($exchange)) {
    $result = db_query("SELECT exchange FROM {stockapi} WHERE symbol = :symbol", array(':symbol' => $field))->fetchField();
    if (empty($result)) {
      $exchange = 'TSE';
    }
    else {
      $exchange = $result;
    }
  }
  switch ($exchange) {
    case 'NYSE':
      $exchange = 'NYSE';
    break;
    case 'TSE':
      $exchange = 'TSE';
    break;
    case 'Toronto Stock Exchange':
      $exchange = 'TSE';
    break;
    case 'New York Stock Exchange':
      $exchange = 'NYSE';
    break;
    case 'NASDAQ':
      $exchange = 'NASDAQ';
    break;
    case '':
      $exchange = 'TSE';
    break;
    case NULL:
      $exchange = 'TSE';
    break;
    default:
      $exchange = 'TSE';
    break;
  }
  return l(drupal_strtoupper($field),
  sprintf('http://finance.google.com/finance?q=%s',
    $exchange . ':' . $field),
  array('attributes' =>
    array('target' => '_blank',
           'title' => (drupal_strlen($title)) ? $title : $field)
    )
  );
}

/**
 * Right-align data in the table cell
 *
 * @param string $field
 * @return string
 */
function stockapi_format_align($field) {
  return '<div align="right">' . $field . '</div>';
}

/**
 * Format decimals
 *
 * @param string $field
 * @return double
 */
function stockapi_format_decimals($field) {
  return number_format(doubleval($field), variable_get('stockapi_decimals', 2));
}

/**
 * Style the quote to show whether there's been an upward or downward trend
 * on the stock quote
 *
 * @param string $field
 * @return string
 */
function stockapi_format_change($field) {
  // Add a style for the change field, so we can
  // add color, up tick/down tick, ...etc.
  $style = 'plus';
  if ($field < 0) {
    $style = 'minus';
  }
  return '<div class="' . $style . '">' . $field . '</div>';
}

